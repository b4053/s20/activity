console.log("Hello")

let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]

console.log('Result from array of objects');
console.log(batchesArr);

//before sending data, use stringify to convert Objects into a JSON.
console.log('Result from stringify method');
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data)



//Other example
let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city belong to?')
}


let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData)


//JSON.parse

let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch Y"
	}
]`

console.log(batchesJSON);

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));
